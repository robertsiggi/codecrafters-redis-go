package main

import (
	"fmt"
	"path/filepath"
	"strconv"
	"strings"
	"time"
)

type SetArgs struct {
	options map[string]string
	key     string
	value   string
}

var db = make(map[string]string)

var (
	dir        = ""
	dbFileName = ""
)

func SetDir(dirArg string) {
	dir = dirArg
}

func GetDir() string {
	return dir
}

func SetDbFileName(dbFileNameArg string) {
	dbFileName = dbFileNameArg
}

func GetDbFileName() string {
	return dbFileName
}

func GetDbFilePath() string {
	return filepath.Join(dir, dbFileName)
}

func Set(args SetArgs) error {
	db[args.key] = args.value

	if len(args.options) <= 0 {
		return nil
	}

	for optionKey, optionValue := range args.options {
		switch strings.ToUpper(optionKey) {
		case "PX":
			delay, err := strconv.Atoi(optionValue)
			if err != nil {
				return fmt.Errorf("invalid value %s for option %s", optionValue, optionKey)
			}
			go expiry(delay, args.key)
		}
	}

	return nil
}

func Get(key string) (string, error) {
	value, ok := db[key]

	if ok {
		return value, nil
	}

	return "", fmt.Errorf("key %s not found", key)
}

func expiry(delayMs int, key string) {
	delay := time.Duration(delayMs) * time.Millisecond

	time.AfterFunc(delay, func() {
		fmt.Println("Deleting key", key)
		delete(db, key)
	})
}
