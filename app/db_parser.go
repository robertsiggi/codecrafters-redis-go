package main

import (
	"bufio"
	"encoding/binary"
	"fmt"
	"os"
	"strconv"
	"time"
)

const (
	aux          = byte(0xfa)
	resizedb     = byte(0xfb)
	expiretimems = byte(0xfc)
	expiretime   = byte(0xfd)
	selectdb     = byte(0xfe)
	eof          = byte(0xff)
)

type DatabaseRow struct {
	key       string
	value     string
	isExpired bool
}

func GetRows() ([]DatabaseRow, error) {
	reader, err := openDb()
	if err != nil {
		return nil, err
	}

	rows := []DatabaseRow{}

	magic := make([]byte, 5)
	redisVersion := make([]byte, 4)
	reader.Read(magic[:])
	reader.Read(redisVersion[:])

	fmt.Println("Magic: ", string(magic[:]))
	fmt.Println("Version: ", string(redisVersion[:]))

	for {
		opcode, err := reader.ReadByte()
		if err != nil {
			return nil, fmt.Errorf("failed to read opcode: %s", err)
		}

		switch opcode {
		case aux:
			err := readAux(reader)
			if err != nil {
				return nil, err
			}
		case resizedb:
			err := readResizeDb(reader)
			if err != nil {
				return nil, err
			}
		case expiretimems:
			row, err := readExpireTimeMs(reader)
			if err != nil {
				return nil, err
			}

			if !row.isExpired {
				rows = append(rows, row)
			}
		case expiretime:
			row, err := readExpireTime(reader)
			if err != nil {
				return nil, err
			}

			if !row.isExpired {
				rows = append(rows, row)
			}
		case selectdb:
			err := readSelectDb(reader)
			if err != nil {
				return nil, fmt.Errorf("error reading db no %s", err)
			}
		case eof:
			return rows, nil

		default:
			row, err := readKeyValue(opcode, reader)
			if err != nil {
				return nil, err
			}

			rows = append(rows, row)
		}
	}
}

func readAux(reader *bufio.Reader) error {
	fmt.Println("AUX")
	key, err := readStringValue(reader)
	if err != nil {
		return err
	}

	value, err := readStringValue(reader)
	if err != nil {
		return err
	}
	fmt.Printf("key/value: %s / %s\r\n", key, value)

	return nil
}

func readResizeDb(reader *bufio.Reader) error {
	fmt.Println("RESIZEDB")
	hashTableSize, err := readIntValue(reader)
	if err != nil {
		return err
	}

	hashTableExpireSize, err := readIntValue(reader)
	if err != nil {
		return err
	}

	fmt.Println("Hash Table Size: ", hashTableSize)
	fmt.Println("Hash Table Expire Size: ", hashTableExpireSize)

	return nil
}

func readExpireTimeMs(reader *bufio.Reader) (DatabaseRow, error) {
	fmt.Println("EXPIRETIMEMS")

	valueBytes := make([]byte, 8)
	_, err := reader.Read(valueBytes)
	if err != nil {
		return DatabaseRow{}, err
	}

	expireTime := int64(binary.LittleEndian.Uint64(valueBytes))
	fmt.Println("expireTimeMS: ", expireTime)

	return readExpireTimeKeyValue(reader, time.Unix(expireTime/1000, (expireTime%1000)*int64(time.Millisecond)))
}

func readExpireTime(reader *bufio.Reader) (DatabaseRow, error) {
	fmt.Println("EXPIRETIME")

	valueBytes := make([]byte, 4)
	_, err := reader.Read(valueBytes)
	if err != nil {
		return DatabaseRow{}, err
	}

	expireTime := int64(binary.BigEndian.Uint32(valueBytes))

	return readExpireTimeKeyValue(reader, time.Unix(expireTime, 0))
}

func readExpireTimeKeyValue(reader *bufio.Reader, expireTime time.Time) (DatabaseRow, error) {
	_, err := reader.ReadByte() // ignore format
	if err != nil {
		return DatabaseRow{}, err
	}

	key, err := readStringValue(reader)
	if err != nil {
		return DatabaseRow{}, err
	}

	value, err := readStringValue(reader)
	if err != nil {
		return DatabaseRow{}, err
	}

	fmt.Println("key/value: ", key, value)

	isExpired := expireTime.Before(time.Now())
	row := DatabaseRow{key, value, isExpired}

	fmt.Println("row: ", row)

	return row, nil
}

func readSelectDb(reader *bufio.Reader) error {
	fmt.Println("SELECTDB")
	no, err := reader.ReadByte()
	if err != nil {
		return fmt.Errorf("error reading db no %s", err)
	}
	fmt.Println("database no: ", no)

	return nil
}

func readKeyValue(format byte, reader *bufio.Reader) (DatabaseRow, error) {
	row := DatabaseRow{}

	switch format {
	case 0:
		key, err := readStringValue(reader)
		if err != nil {
			return row, err
		}

		value, err := readStringValue(reader)
		if err != nil {
			return row, err
		}

		fmt.Printf("key/value: %s / %s\r\n", key, value)

		return DatabaseRow{key, value, false}, nil
	}
	return row, fmt.Errorf("unknown format %d", format)
}

func readStringValue(reader *bufio.Reader) (string, error) {
	firstByte, err := reader.ReadByte()
	if err != nil {
		return "", fmt.Errorf("failed to read first byte: %s", err)
	}

	msbs := firstByte >> 6

	switch msbs {
	case 0:
		len := int(firstByte)
		valueBytes := make([]byte, len)
		_, err := reader.Read(valueBytes)
		if err != nil {
			return "", fmt.Errorf("failed to read value: %s", err)
		}
		return string(valueBytes), nil
	case 1:
		fmt.Println("x1")
	case 2:
		fmt.Println("x2")
	case 3:
		format := firstByte & 0x3f
		switch format {
		case 0:
			value, err := reader.ReadByte()
			if err != nil {
				return "", fmt.Errorf("failed to read value: %s", err)
			}
			return strconv.Itoa(int(value)), nil
		}

	default:
		fmt.Println("???")
	}

	return "", nil
}

func readIntValue(reader *bufio.Reader) (int, error) {
	firstByte, err := reader.ReadByte()
	if err != nil {
		return 0, fmt.Errorf("failed to read first byte: %s", err)
	}

	msbs := firstByte >> 6

	switch msbs {
	case 0:
		return int(firstByte), nil
	case 1:
		secondByte, err := reader.ReadByte()
		if err != nil {
			return 0, fmt.Errorf("failed to read value: %s", err)
		}

		remaining := firstByte & 0x3f
		return int(remaining<<6) + int(secondByte), nil
	default:
		return -1, nil
	}
}

func openDb() (*bufio.Reader, error) {
	file, err := os.Open(GetDbFilePath())
	if err != nil {
		return nil, fmt.Errorf("failed to open db: %s", err)
	}

	return bufio.NewReader(file), nil
}
