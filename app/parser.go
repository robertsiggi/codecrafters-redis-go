package main

import (
	"bufio"
	"fmt"
	"io"
	"strconv"
	"strings"
)

func parseInput(reader *bufio.Reader) (RedisCommand, error) {
	redisCommand := RedisCommand{}

	firstLine, err := readLine("\r\n", reader)
	if err != nil {
		return redisCommand, fmt.Errorf("error reading first line: %s", err.Error())
	}

	expectedItemCnt, err := extractExpectedCnt("*", firstLine)
	if err != nil {
		return redisCommand, err
	}

	for i := 0; i < expectedItemCnt; i++ {
		argSizeLine, err := readLine("\r\n", reader)
		if err != nil {
			if err != io.EOF {
				return redisCommand, fmt.Errorf("error reading line: %s", err.Error())
			}
			fmt.Println("EOF")
			break
		}

		expectedLineSize, err := extractExpectedCnt("$", argSizeLine)
		if err != nil {
			return redisCommand, err
		}

		line, err := readLine("\r\n", reader)
		if err != nil {
			return redisCommand, fmt.Errorf("error reading arg: %s", err.Error())
		}

		line = strings.TrimSpace(line)

		if len(line) != expectedLineSize {
			return redisCommand, fmt.Errorf("expected arg size %d, got %d", expectedLineSize, len(line))
		}

		if i == 0 {
			fmt.Println("parsed command", line)
			redisCommand.Command = strings.ToUpper(line)
			continue
		}

		fmt.Println("parsed arg", line)
		redisCommand.Args = append(redisCommand.Args, line)
	}

	return redisCommand, nil
}

func readLine(delimiter string, reader *bufio.Reader) (string, error) {
	lastChar := delimiter[len(delimiter)-1]

	fullLine := ""

	for {
		line, err := reader.ReadString(lastChar)
		if err != nil {
			if err == io.EOF {
				fmt.Printf("EOF")
				if len(fullLine) == 0 {
					return "", io.EOF
				}
				break
			}
			return "", fmt.Errorf("could not find delimiter %s in line: %s", delimiter, line)
		}
		fullLine += line

		if strings.HasSuffix(fullLine, delimiter) {
			break
		}
	}

	return fullLine, nil
}

func extractExpectedCnt(prefix string, input string) (int, error) {
	cleanedInput := strings.TrimSpace(input)

	if len(cleanedInput) == 0 || !strings.HasPrefix(cleanedInput, prefix) {
		return 0, fmt.Errorf("invalid command format. expected *<number><CR><LF>. Received: %s", cleanedInput)
	}

	number, err := strconv.Atoi(cleanedInput[1:])
	if err != nil {
		return 0, err
	}

	return number, nil
}
