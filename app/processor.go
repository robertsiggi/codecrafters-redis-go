package main

import (
	"bufio"
	"fmt"
	"strconv"
	"strings"
)

func processCommand(command RedisCommand, writer *bufio.Writer) error {
	fmt.Println("command", command)
	switch command.Command {
	case "PING":
		return processCommandPing(writer)
	case "ECHO":
		return processCommandEcho(command.Args, writer)
	case "SET":
		return processCommandSet(command.Args, writer)
	case "GET":
		return processCommandGet(command.Args, writer)
	case "CONFIG":
		return processCommandConfig(command.Args, writer)
	case "KEYS":
		return processCommandAllKeys(command.Args, writer)
	default:
		return fmt.Errorf("unknown command: %s", command.Command)
	}
}

func processCommandPing(writer *bufio.Writer) error {
	fmt.Println("Processing PONG")
	_, err := writer.WriteString("+PONG\r\n")
	if err != nil {
		return fmt.Errorf("error writing PONG response: %s", err.Error())
	}
	writer.Flush()
	return nil
}

func processCommandEcho(args []string, writer *bufio.Writer) error {
	fmt.Println("Processing ECHO", args)
	_, err := writer.WriteString("+" + args[0] + "\r\n")
	if err != nil {
		return fmt.Errorf("error writing echo response: %s", err.Error())
	}
	writer.Flush()
	return nil
}

func processCommandSet(args []string, writer *bufio.Writer) error {
	fmt.Println("Processing SET", args)

	setArgs := SetArgs{
		key:     args[0],
		value:   args[1],
		options: map[string]string{},
	}

	if len(args) > 2 {
		for i := 2; i < len(args); i++ {
			optionKey := args[i]
			if i < len(args)-1 {
				setArgs.options[optionKey] = args[i+1]
				i++
			} else {
				setArgs.options[optionKey] = ""
			}
		}
	}

	Set(setArgs)

	_, err := writer.WriteString("+OK\r\n")
	if err != nil {
		return fmt.Errorf("error writing SET response: %s", err.Error())
	}
	writer.Flush()
	return nil
}

func processCommandGet(args []string, writer *bufio.Writer) error {
	fmt.Println("Processing GET", args)

	value, err := Get(args[0])
	if err != nil {
		success := processCommandKeys(args, writer)
		if success == nil {
			return nil
		}
		_, err = writer.WriteString("$-1\r\n")
		if err != nil {
			return fmt.Errorf("error writing GET response: %s", err.Error())
		}
		writer.Flush()
		return err
	}

	fmt.Println("value", value)

	_, err = writer.WriteString("$" + strconv.Itoa(len(value)) + "\r\n" + value + "\r\n")
	if err != nil {
		return fmt.Errorf("error writing GET response: %s", err.Error())
	}
	writer.Flush()
	return nil
}

func processCommandConfig(args []string, writer *bufio.Writer) error {
	fmt.Println("Processing CONFIG", args)

	getCommand := args[0]
	if strings.ToUpper(getCommand) != "GET" {
		return fmt.Errorf("expected get command, got %s", args)
	}

	subCommand := args[1]

	switch strings.ToUpper(subCommand) {
	case "DIR":
		dir := GetDir()
		result := "*2\r\n$3\r\ndir\r\n$" + strconv.Itoa(len(dir)) + "\r\n" + dir + "\r\n"
		_, err := writer.WriteString(result)
		if err != nil {
			return fmt.Errorf("error writing CONFIG response: %s", err)
		}
		writer.Flush()
	}

	return nil
}

func processCommandAllKeys(args []string, writer *bufio.Writer) error {
	fmt.Println("Processing all KEYS", args)

	rows, err := GetRows()
	if err != nil {
		return err
	}

	cnt := len(rows)
	result := "*" + strconv.Itoa(cnt) + "\r\n"
	for _, row := range rows {
		result += "$" + strconv.Itoa(len(row.key)) + "\r\n" + row.key + "\r\n"
	}

	_, err = writer.WriteString(result)
	if err != nil {
		return fmt.Errorf("error writing KEYS response: %s", err)
	}
	writer.Flush()

	return nil
}

func processCommandKeys(args []string, writer *bufio.Writer) error {
	fmt.Println("Processing KEYS", args)

	rows, err := GetRows()
	if err != nil {
		return err
	}

	resultValue := ""

	for _, row := range rows {
		if row.key == args[0] {
			resultValue = row.value
			break
		}
	}

	if resultValue == "" {
		fmt.Println("return emptry string")
		_, err = writer.WriteString("$-1\r\n")
	} else {
		result := "$" + strconv.Itoa(len(resultValue)) + "\r\n" + resultValue + "\r\n"
		fmt.Println("return string: ", result)
		_, err = writer.WriteString(result)
	}
	if err != nil {
		return fmt.Errorf("error writing KEYS response: %s", err)
	}
	writer.Flush()

	return nil
}
