package main

import (
	"bufio"
	"flag"
	"fmt"
	"net"
	"os"
)

func main() {
	fmt.Println("Starting Redis Server")

	dir := flag.String("dir", "", "The directory where RDB files are stored")
	dbfilename := flag.String("dbfilename", "", "The name of the RDB file")

	flag.Parse()

	fmt.Println("dir:", *dir)
	fmt.Println("dbfilename:", *dbfilename)

	SetDir(*dir)
	SetDbFileName(*dbfilename)

	listener, err := net.Listen("tcp", "0.0.0.0:6379")
	if err != nil {
		fmt.Println("Failed to bind to port 6379")
		os.Exit(1)
	}

	for {
		fmt.Println("Waiting for connection")

		connection, err := listener.Accept()
		if err != nil {
			fmt.Println("Error accepting connection: ", err.Error())
			os.Exit(1)
		}

		go processConnection(connection)

	}

	// fmt.Print("Server stopped")
}

func processConnection(connection net.Conn) {
	defer connection.Close()

	for {

		fmt.Println("Processing connection", connection.RemoteAddr())

		reader := bufio.NewReader(connection)
		writer := bufio.NewWriter(connection)

		redisCommand, err := parseInput(reader)
		if err != nil {
			fmt.Println(err)
			return
		}

		err = processCommand(redisCommand, writer)
		if err != nil {
			fmt.Println(err)
			return
		}
	}
}
